<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags Obrigatórias -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <title>Livraria - MHSystem</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand text-primary" href="index.html">LIVRARIA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ml-sm-2">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Consulta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Painel Administrativo</a>
                    </li>
                </ul>
            </div>
        </nav>
        <h1>Bem-Vindo à nossa Livraria</h1>


        <div class="sagas"> 
            <h4>Sagas</h4>
            <div class="blocosaga">
                <div class="nomesaga">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocosaga">
                <div class="nomesaga">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocosaga">
                <div class="nomesaga">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocosaga">
                <div class="nomesaga">
                    <h6>NOME</h6>
                </div>
            </div> 
            <div class="blocosaga">
                <div class="nomesaga">
                    <h6>NOME</h6>
                </div>
            </div>

        </div>
        <div class="maisvendidos">
            <h4>Mais Vendidos</h4>
            <div class="blocomaisvendidos">
                <div class="internablocovendidos"></div>
                <div class="nomelivrovendido"><h6>A História do Espetinho</h6> </div>
                <div class="autorlivrovendido"><h6>Escrito por Matheus</h6> </div>
                <div class="valorvendido"><h4>R$200,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocomaisvendidos">
                <div class="internablocovendidos"></div>
                <div class="nomelivrovendido"><h6>Como Solucionar as dores</h6> </div>
                <div class="autorlivrovendido"><h6>Escrito por Felipe</h6> </div>
                <div class="valorvendido"><h4>R$2000,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocomaisvendidos">
                <div class="internablocovendidos"></div>
                <div class="nomelivrovendido"><h6>Arduino pode dominar o mundo</h6> </div>
                <div class="autorlivrovendido"><h6>Escrito por Luiz</h6> </div>
                <div class="valorvendido"><h4>R$400,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocomaisvendidos">
                <div class="internablocovendidos"></div>
                <div class="nomelivrovendido"><h6>E se a pessoa não saber ler</h6> </div>
                <div class="autorlivrovendido"><h6>Escrito por Anonimo</h6> </div>
                <div class="valorvendido"><h4>R$900,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>


        </div>


        <div class="populares">
            <h4>Populares</h4>
            <div class="blocopopulares">
                <div class="internablocopopulares"></div>
                <div class="nomepopulares"><h6>Sonhar</h6> </div>
                <div class="autorlivropopular"><h6>Escrito por Matheus</h6> </div>
                <div class="valorvendido"><h4>R$200,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocopopulares">
                <div class="internablocopopulares"></div>
                <div class="nomepopulares"><h6>Hsf a Empresa do futuro</h6> </div>
                <div class="autorlivropopular"><h6>Escrito por Felipe</h6> </div>
                <div class="valorvendido"><h4>R$1000,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocopopulares">
                <div class="internablocopopulares"></div>
                <div class="nomepopulares"><h6>Arduino pode levar ate a lua</h6> </div>
                <div class="autorlivropopular"><h6>Escrito por Luiz</h6> </div>
                <div class="valorvendido"><h4>R$500,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>
            <div class="blocopopulares">
                <div class="internablocopopulares"></div>
                <div class="nomepopulares"><h6>Como nao gastar</h6> </div>
                <div class="autorlivropopular"><h6>Escrito por Anonimo</h6> </div>
                <div class="valorvendido"><h4>R$100,00</h4></div>
                <button class="adicionarcarrinho"><h6>ADICIONAR AO CARRINHO</h6></button>
            </div>

        </div>
        <div class="editoras"> 
            <h4>Editoras</h4>
            <div class="blocoedititora">
                <div class="nomeeditora">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocoedititora">
                <div class="nomeeditora">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocoedititora">
                <div class="nomeeditora">
                    <h6>NOME</h6>
                </div>
            </div>
            <div class="blocoedititora">
                <div class="nomeeditora">
                    <h6>NOME</h6>
                </div>
            </div> 
            <div class="blocoedititora">
                <div class="nomeeditora">
                    <h6>NOME</h6>
                </div>
            </div>

        </div>
        <div class="promocao">
            <h4>Promoção</h4>
            <div class="blocopromocao"></div>
            <div class="blocopromocao"></div>
            <div class="blocopromocao"></div>
            <div class="blocopromocao"></div>
            
            
            
        </div>

        <div class="container mt-2">

            <div class="row">
                <div class="col-12">

                    <p class="text-secondary ml-1">Nossos produtos:</p>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="card text-center">
                        <img class="card-img-top" src="img/casa.png">
                        <div class="card-body">
                            <h5 class="card-title">Livro 1</h5>
                            <p class="card-text">Descrição</p>
                            <a href="#" class="btn btn-primary">Comprar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mt-3 mt-md-0">
                    <div class="card text-center">
                        <img class="card-img-top" src="img/casa.png">
                        <div class="card-body">
                            <h5 class="card-title">Livro 2</h5>
                            <p class="card-text">Descrição</p>
                            <a href="#" class="btn btn-primary">Comprar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mt-3 mt-lg-0">
                    <div class="card text-center">
                        <img class="card-img-top" src="img/casa.png">
                        <div class="card-body">
                            <h5 class="card-title">Livro 3</h5>
                            <p class="card-text">Descrição</p>
                            <a href="#" class="btn btn-primary">Comprar</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mt-3 mt-lg-0">
                    <div class="card text-center">
                        <img class="card-img-top" src="img/casa.png">
                        <div class="card-body">
                            <h5 class="card-title">Livro 4</h5>
                            <p class="card-text">Descrição</p>
                            <a href="#" class="btn btn-primary">Comprar</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="js/jquery.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>