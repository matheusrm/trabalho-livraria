<?php ?>
<html lang="pt-br">
    <head>
        <!-- Meta tags Obrigatórias -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="js/jquery.js"></script>
        <script src="js/popper.js"></script>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="bootstrap.css">

        <title>Painel de controle - MHSystem</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand text-primary" href="index.html">LIVRARIA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" 
                    data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação"></button>
            <span class="navbar-toggler-icon"></span>
            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ml-sm-2">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Consulta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Painel Administrativo</a>
                    </li>
                </ul>
            </div>
        </nav>
        <h1>Bem-Vindo ao Painel Administrativo</h1>
        <div class="topocadastro"> <h4>Cadastro de Livro</h4></div>
        <div class="cadastrar">
            <div class="nomecadastro">  <h5>Nome do Livro</h5>
                <input type="text" name="nome"  class="form-control" 
                       placeholder="Nome do livro">  </div>
            <div class="foto"> <h5>Imagem do Livro</h5> 
                <input type="image" name="nome"  
                       class="form-control" placeholder="Imagem do livro"> </div>
            <div class="autor">  <h5>Autor do Livro</h5> 
                <input type="text" name="nome"  
                       class="form-control" placeholder="Nome do Autor"> </div>
            <div class="preco">  <h5>Preço do Livro</h5>
                <input type="text" name="nome"  
                       class="form-control" placeholder="Preço"> </div>
            <div class="descricao">  <h5>Descrição do Livro</h5>
                <textarea class="descricaotexto"></textarea>
            </div>

            <button name="botao" value="cadastrar" 
                    type="submit" class="btn btn-primary" id="cadastrar" >Cadastrar</button>

        </div>
        <div class="topocadastro"> <h4>Auteração de Livro</h4></div>
        <div class="atualizar">
            <div class="nomeatualizar">
                <h5>Nome do Livro </h5>
                <input type="text" name="atualizarnome"  class="form-control" 
                       placeholder="Nome do livro"> 
            </div>

            <div class="imgatualizar">
                <h5>Imagem do Livro </h5>
                <input type="image" name="atualizarimagem"  class="form-control" 
                       placeholder="Nome do livro">  </div>

            <div class="autoratualizar">
                <h5>Nome do Autor </h5>
                <input type="text" name="atualizarautor"  class="form-control" 
                       placeholder="Nome do livro"> 
            </div>
            <div class="precoatualizar">
                <h5>Preço do Livro </h5>
                <input type="text" name="atualizarpreco"  class="form-control" 
                       placeholder="Nome do livro"> 
            </div>
                        <div class="descricao">  <h5>Descrição do Livro</h5>
                <textarea class="descricaotexto"></textarea>
            </div>

            <div class="botoes">
                <button name="botao" value="atualizar" 
                        type="submit" class="btn btn-atualiza" id="atualizar" >Atualizar</button>
                <button name="botao" value="excluir" 
                        type="submit" class="btn btn-exclui" id="excluir" >Excluir</button>
            </div>

        </div>

    </body>
</html>


















